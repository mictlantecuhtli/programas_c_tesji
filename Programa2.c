/*
 * Nombre: Fátima Azucena MC
 * Fecha: 17_08_22
 * Correo electronico: fatimaazucenamartinez274@gmail.com
*/

# include <stdio.h>

int main(){

	char nombre[20];
	int A;
	int n;

	printf("Introduzca su nombre completo: ");
	scanf("%s", &nombre);

	printf("¿Cuántas veces desea que se muestre su nombre?: ");
	scanf("%d", &n);

	A = 1;

	while ( A <= n ){
	
		printf("Tu nombre completo es: %s", nombre);
		printf("\n");
		A++;
	
	}



}
