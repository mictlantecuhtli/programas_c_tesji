/*
 * Autor: Fátima Azucena Martínez Cadena
 * Fecha: 12_12_22
 * Correo: fatimaazucenamartinez274@gmail.com
*/

#include <stdio.h>

int main() {

	//Declaración de variables
	int calificaciones[6][5];
	char  materias [][50] = {"Cálculo Diferencial","Fundamentos de Programación","Química","Fundamentos de Investigación","Matemáticas Discretas"
	,"Desarrollo Sustentable"};
	char unidad [][50] = {"Unidad 1","Unidad 2","Unidad 3","Unidad 4","Unidad 5"};
	int i;
	int y;
	int a;
	int b;
	int promedioFinal;
	int promedioUnidades = 0;

	for ( i = 0; i < 6; i++ ) {
		int promedioMateria = 0;
		int sumaCalificaciones = 0;
		for ( y = 0; y < 5; y++ ) {
			printf("Ingrese la calificación de la %s%s%s%s" , unidad[y] , " de la materia " , materias[i] , ": ");
			scanf("%d" , &calificaciones[i][y]);
			sumaCalificaciones += calificaciones[i][y];
		}
		promedioMateria = sumaCalificaciones / 5;
		promedioUnidades += promedioMateria;
		printf("El promedio de la materia %s%s%d%s" , materias[i] , " es: " , promedioMateria , "\n\n");
	}
	promedioFinal = promedioUnidades / 6;
	printf("El promedio general es de: %d" , promedioFinal);
}
