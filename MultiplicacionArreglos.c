/*
 * Autor: Fátima Azucena Martínez Cadena
 * Fecha: 12_12_22
 * Correo: fatimaazucenamartinez274@gmail.com
*/

#include <stdio.h>

int main(){

	//Declaración de variables
	int a1[5];
	int a2[5];
	int result[5];
	int i;
	int y;
	int a;
	int c;
	int z;
	int b = 4;

	printf("\n\nArreglo 1 \n");
	for ( i = 0; i < 5; i++ ) {
		printf("Ingrese el número en la posición %d%s" , i , ": ");
		scanf("%d", &a1[i]);
	}
	for ( c = 0; c < 5; c++ ) {
                printf("%d%s" , a1[c] , " ");
        }
	printf("\n\nArreglo 2 \n");
        for ( y = 0; y < 5; y++ ) {
                printf("Ingrese el número en la posición %d%s" , y , ": ");
                scanf("%d", &a2[y]);
        }
	 for ( z = 0; z < 5; z++ ) {
                printf("%d%s" , a2[z] , " ");
        }
	printf("\n\nResultado \n");
        for ( a = 0; a < 5; a++ ) {
                result [a] = a1[a]*a2[b];
		b--;
		printf("%d%s" , result[a] , " ");
        }
}
