/*
 * Nombre: Fátima Azucena MC
 * Fecha: 17_08_22
 * Correo electronico: fatimaazucenamartinez274@gmail.com
*/

# include <stdio.h>

int main(){

	int tabla;
	int tope;
	int A;
	int resultado;

	printf("Digite la tabla que desee: ");
	scanf("%d", &tabla);

	printf("Digite hasta donde desea parar: ");
	scanf("%d" , &tope);

	A = 1;

	while ( A <= tope) {
	
		resultado = tabla * A;

		printf("%d %s %d %s %d" , tabla , " x ", A , " = ", resultado );
		printf("\n");
		A++;
	
	}

}
