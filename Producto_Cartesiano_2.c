/*
 * Autor: Fátima Azucena MC
 * Fecha: 18_10_22
 * Correo: fatimaazucenamartinez274@gmail.com
*/

#include <stdio.h>

int main(){

	int x;
	int y;
	int a;
	int i;
	int d;
	int z;
	int f;
	int impar;
	int ingreso;
	int tamA;
	int tamA2;
	int tamB;
	int tamB2;
	int respuesta;
	int g;
	int h;
	int j;
	int k;

	printf("\nBienvenido, este programa te ayudará a obtener el producto cartesiano de dos conjuntos");
	printf("\n¿Desea separar sus números en pares e impares?:\n1)Si\n0)No\nDigite su respuesta: ");
	scanf("%d", &respuesta);

	if ( respuesta == 1){

		printf("Ingrese el tamaño del conjunto A: ");
		scanf("%d", &tamA);	

		printf("Ingrese el tamaño del conjunto B: ");
        	scanf("%d", &tamB);
        
		a = tamA + tamB;
		int c[a]; 
		x = 0;
		y = a - 1;

		for ( i = 0; i < a; i++ ){
                	printf("\nIngrese el valor de la posición %d: ", i);
			scanf("%d", &ingreso);
               
			if ( ingreso % 2 == 0 ) {
				c[x] = ingreso;
				x++;
			}

			else {
				c[y] = ingreso;
				y--;
			}
		}

		d = x;
		printf("\nAxB={");
		for ( f = 0; f < x; f++){
			for ( z = d; z < a; z++ ){
				printf("\n(%d %s %d %s",c[f],",",c[z],")\n");
			}
		}
		printf("}\n");
	}
	else {
		printf("\nIngrese el tamaño del conjunto A: ");
                scanf("%d", &tamA2);
		int arregloA[tamA2];

                printf("Ingrese el tamaño del conjunto B: ");
                scanf("%d", &tamB2);
		int arregloB[tamB2];

		printf("\n");
		for ( j = 0; j < tamA2; j++) {
			printf("Ingrese el valor en la posición %d: ", j);
			scanf("%d", &arregloA[j]);
		}

		printf("\n");
		for ( k = 0; k < tamB2; k++) {
			printf("Ingrese el valor en la posición %d: ", k);
      	                scanf("%d", &arregloB[k]);
		}

		printf("\nAxB={");
                for ( g = 0; g < tamA2; g++){
                        for ( h = 0; h < tamB2; h++ ){
                                printf("\n(%d %s %d %s",arregloA[g],",",arregloB[h],")\n");
                        }
                }
                printf("}\n");
	}
}
