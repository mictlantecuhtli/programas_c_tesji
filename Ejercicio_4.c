/*
 * Autor: Fatima Azucena MC
 * Fecha: 10_11_22
 * Correo: fatimaazucenamartinez274@gmail.com
*/

#include <stdio.h>
int main (){

	int opcion;
	int cantidad;
	int total;

	printf("Tallas disponibles:\n1.-Talla chica\n2.-Talla mediana\n3.-Talla grande\nDigite su opcion: ");
	scanf("%d", &opcion );

	switch (opcion) {
		case 1:
			printf("\nA elegido talla chica (Talla 7)\n¿Cuántos vestidos compró?: ");
			scanf("%d", &cantidad);
			total = cantidad * 300;
		break;
		case 2:
			printf("\nA elegido talla mediana (Talla 12)\n¿Cuántos vestidos compró?: ");
                        scanf("%d", &cantidad);
                        total = cantidad * 400;
		break;
		case 3:
			printf("\nA elegido talla grande (Talla 16)\n¿Cuántos vestidos compró?: ");
                        scanf("%d", &cantidad);
                        total = cantidad * 500;
		break;
		default:
			printf("\nOpción inválida");
	}

	printf("\nEl total a pagar es: %d %s", total, "\n");

}
