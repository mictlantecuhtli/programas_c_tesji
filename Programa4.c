/*
 * Nombre: Fátima Azucena MC
 * Fecha: 17_08_22
 * Correo electronico: fatimaazucenamartinez274@gmail.com
*/

# include <stdio.h>

int main(){

	int n;
	int fact;
	int A;
	int b;

	printf("Digite hasta que número desea ver su factorial: ");
	scanf("%d", &n);

	A = 1;
	fact = 1;

	while ( A <= n ){
	
		b = fact * A;

		printf("%d %s %d %s %d", fact, " x ", A , " = ", b);
		printf("\n");

		fact = fact * A;
		A++;

	}

}
