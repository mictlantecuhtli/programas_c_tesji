/*
 * Nombre: Fátima Azucena MC
 * Fecha: 17_08_22
 * Correo electronico: fatimaazucenamartinez274@gmail.com
*/

# include <stdio.h>

int main(){

	int n;
	int A;
	int suma = 0;

	printf("Digite un número: ");
	scanf("%d", &n);

	A = 1;

	while ( A <= n ){
		
		suma = suma + A;
		A++;

	}
	
	printf("\n");
	printf("%d", suma);
	printf("\n");

}
