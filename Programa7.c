/*
 * Nombre: Fátima Azucena MC
 * Fecha: 17_08_22
 * Correo electronico: fatimaazucenamartinez274@gmail.com
*/

# include <stdio.h>

int main(){

	int a;
	int b;
	int resultado;

	a = 1;

	while ( a <= 15 ){
		
		b = 1;

		while ( b <= 15 ){
		
			resultado = a * b;
			printf("%d %s %d %s %d %s", a," x ", b," = ", resultado,"\n");
			b++;

		}

		printf("%s","\n");
		a++;
	
	}


}
