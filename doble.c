/*
 * Autor: Fatima Azucena MC
 * Fecha: 09_09_22
 * Correo electronico: fatimaazucenamartinez274@gmail.com
*/

#include <stdio.h>

int main(){

	int numero, doble, tercera, mitad;
	numero = 30;
	doble = numero * 2;
	tercera = doble / 3;
	mitad = tercera / 2;

	printf("La mitad de la tercera parte del doble de 30: %d", mitad);

}
