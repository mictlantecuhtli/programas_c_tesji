/*
* Fecha: 29_09_22
* Autor: Fátima Azucena MC
* Correo: fatimaazucenamartinez274@gmail.com
*/

#include <stdio.h>

int main(){

	int anyos;
	int dias;
	int diasExtras;

	printf("¿Cuántos años llevas trabajando?: ");
	scanf("%d", &anyos);


	if ( anyos < 1 ){
	
		printf("Lo sentimos, no tienes un año minimo para vacaciones");
	
	}
	else if ( anyos >= 1 && anyos <= 5 ) {
	
		printf("Cuentas con 5 días de vacaciones");

	}
	else if ( anyos >=5 && anyos <= 10 ) {
	
		printf("Cuentas con 8 días de vacaciones");

	}
	else if ( anyos > 10 ) {
	
		dias = anyos - 10;
		diasExtras = 8 + dias;

		printf("Cuentas con %d %s", diasExtras, " días de vacaiones");
	
	}

}
